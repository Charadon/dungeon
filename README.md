<html>
	<body>
		<center>
			<font size="8">
				<b>
					<img src="DungeonBanner.svg" width="800px" height="300px">
				</b>
			</font>
		</center>
		<hr />
			<center>
				<b>
					Description:
				</b>
			</center>
			<br />
			<b>Story:</b> You are a mercenary hired by a local king to take out the snake king. He resides in his tower where many dangers await.
			<br />
			<b>Technical Details:</b> This is my first attempt at making an original game. My code may be a bit (a lot) spagetti, but I think the end result will be very encouraging to myself.
		<hr />		
		<ul>
			<b>
				List of Dependencies:
			</b>
			<li>
				<b>
					ncurses
				</b>	
			</li>
			<li>
				<b>
					raylib
				</b>
			</li>	
		</ul>
		<hr />
		<center>
			<b>
				Build Instructions:
			</b>
		</center>
	   <center>
			<b>Note: Sounds are unused right now.</b>
		</center>
		<ol>
			<b>
				Linux:
			</b>
			<li>
				Install ncurses
			</li>
			<li>
				Install raylib
			</li>
			<li>
				cd into the directory with meson.build
			</li>
			<li>
				Run command: meson build 
			</li>
			<li>
				cd into the generated build directory
			</li>
			<li>
				Run command: ninja
			</li>
			<li>
				Run command: "./dungeon" to run the game.
			</li>
		</ol>
		<ol>
			<b>
				Windows:
			</b>
			<li>
				Install msys2
			</li>
			<li>
				Install the mingw-w64-x86_64-ncurses package.
			</li>
			<li>
				To install raylib, either compile it yourself, or use my PKGBUILD found here: <a href="https://gitlab.com/Charadon/msys2-raylib-bin">Click Here</a>
			</li>
			<li>
				Launch MSYS2 MinGW 64-bit
			</li>
			<li>
				cd into the directory with meson.build
			</li>
			<li>
				Follow linux instructions from here.
			</li>
			<li>
				Run command: cp /mingw64/bin/libwinpthread-1.dll . && cp /mingw64/bin/libraylib.dll . && cp /mingw64/bin/libgcc_s_seh-1.dll .
			</li>
			<li>
				Finally, run the program. NOTE: On versions folder than Windows 10, use powershell to run the game.
			</li>
		</ol>
		<hr />
		FAQ: <br />
		<ol>
			<li>
			Q: How bad is the level 1's code? <br />
			A: Bad. Tons of issues, and will probably be rewritten. I've begun to mark what's legacy code and what isn't.
			</li>
			<li>
			Q: Clang complains about out-of-bound arrays <br />
			A: Yup, that'd be level 1's code. It's harmless, but definitely needs to be fixed eventually.
			</li>
			<li>
			Q: When I resize the terminal, the game breaks.
			A: It's not technically breaking. This game is 100% text based, meaning if you resize the window to where it can't render all text on a line, it'll push it down to the next line. The window should be atleast 80 columns, and 25 lines. In the future, I plan on adding a detector that'll complain to the player if they decide to resize the window and stop the game until they resize it to atleast the requirements stated above.
			</li>
			<li>
			Q: Is there builds available for linux? <br />
			A: Yes, you can download appimages here: <a href="https://download.opensuse.org/repositories/home:/Charadon:/branches:/OBS:/AppImage:/Templates:/Leap:/15.2/AppImage/">Link</a>
			</li>
		</ol>
	</body>
</html>
