/* 
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/
#include "dungeon.h"

//Sound effect not implemented yet. These are placeholders.
void sound_key_get(){
	return;
}

void sound_door_open(){
	PlaySound(dooropening_sound);
	return;
}

void sound_door_locked(){
	PlaySound(doorlocked_sound);
	return;
}

void sound_player_died(){
	PlaySound(playerdead_sound);
	return;
}

void sound_next_level(){
	PlaySound(stairs_sound);
	return;
}

void sound_checkpoint_reached(){
	PlaySound(checkpoint_sound);
	return;
}