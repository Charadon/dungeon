/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "dungeon.h"

void title_screen(){
	int16_t key_press;
	bool title_screen_going = TRUE;
	int8_t menu_selection = 0;
	const int8_t title_text_render[] = {
		0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1,
		0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1,
		0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,
		0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1,
		0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1,
		0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1,
		0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, -1
	};
	while (title_screen_going == TRUE){
		tick_clock();
		//Draws frame
		mvwprintw(stdscr, 0, 0, "+=====================================================+");
		mvwprintw(stdscr, 1, 0, "|                                                     |");
		mvwprintw(stdscr, 2, 0, "|                                                     |");
		mvwprintw(stdscr, 3, 0, "|                                                     |");
		mvwprintw(stdscr, 4, 0, "|                                                     |");
		mvwprintw(stdscr, 5, 0, "|                                                     |");
		mvwprintw(stdscr, 6, 0, "|                                                     |");
		mvwprintw(stdscr, 7, 0, "|                                                     |");
		mvwprintw(stdscr, 8, 0, "+=====================================================+");
		int16_t block_number = 0;
		int8_t block_coordinate[] = {1, 0};
		for ( block_number = 0; title_text_render[block_number] != -1; block_number++ ){
			if ( block_number % 52 == 0 && block_coordinate[1] != 0 ){
				block_coordinate[0]++;
				block_coordinate[1] = 1;
			} else {
				block_coordinate[1]++;
			}

			if ( title_text_render[block_number] == 1 ) {
				attron( COLOR_PAIR(WALL_COLOR) );
					mvwaddch( stdscr, block_coordinate[0], block_coordinate[1], ' ' );
				attroff( COLOR_PAIR(WALL_COLOR) );
			}
		}
		mvprintw(10, 2, "Start");
		mvprintw(11, 2, "Continue");
		mvprintw(12, 2, "Credits");
		mvprintw(13, 2, "Glossary");
		mvprintw(14, 2, "Quit");
		key_press = getch();
		switch(key_press){
			case KEY_UP:
				menu_selection--;
				break;
			case KEY_DOWN:
				menu_selection++;
				break;
			case KEY_RETURN:
				switch(menu_selection){ //Checks which menu option is selected and acts accordingly.
					case 0: //Start
						level_1();
						break;
					case 1: //Continue
						title_choose_level();
						break;
					case 2: //Credits
						title_credits();
						clear();
						break;
					case 3: //Glossary
						title_glossary();
						break;
					case 4: //Quit
						end_game();
						break;
					default:
						break;
				}
			default:
				break;
		}
		//Replaces arrows with whitespaces. This is preferred to just flat out clearing the screen as it prevents graphical jittering.
		mvaddch(10, 0, ' ');
		mvaddch(11, 0, ' ');
		mvaddch(12, 0, ' ');
		mvaddch(13, 0, ' ');
		mvaddch(14, 0, ' ');
		attron(A_BOLD);
		switch(menu_selection){ //Prints where the arrow is on the main menu.
			case 0:
				mvaddch(10, 0, '>'); //Start
				break;
			case 1:
				mvaddch(11, 0, '>'); //Continue
				break;
			case 2:
				mvaddch(12, 0, '>'); //Options
				break;
			case 3:
				mvaddch(13, 0, '>'); //Glossary
				break;
			case 4:
				mvaddch(14, 0, '>'); //Quit
				break;
			case 5:
				menu_selection = 0; //Goes to start
				break;
			case -1:
				menu_selection = 4; //Goes to quit
				break;
			default:
				printf("An error has occured with the menu selection switch.");
				exit(0);
		}
		attroff(A_BOLD);
		refresh();
	}
}

void title_choose_level(){
	int8_t level_chosen;
	mvprintw(11, 12, "Type Level:");
	begin_user_input();
	mvscanw(11, 24, "%i", &level_chosen);
	end_user_input();
	switch(level_chosen){ //Goes to level
		case 1:
			level_1();
			break;
		case 2:
			level2();
			break;
		case 3:
			level3();
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			break;
		case 7:
			break;
		case 8:
			break;
		case 9:
			break;
		case 10:
			break;
		default:
			break;
	}
	//Clears line next to Continue.
	mvprintw(11, 12, "                                    ");
	return;
}

void title_credits(){
	clear();
	mvprintw(0, 0, "+======================================================================+");
	mvprintw(1, 0, "| This program is licensed under the GPL-3.0                           |");
	mvprintw(2, 0, "| See license at git repository:                                       |");
	mvprintw(3, 0, "| https://gitlab.com/Charadon/dungeon                                  |");
	mvprintw(4, 0, "|----------------------------------------------------------------------|");
	mvprintw(5, 0, "| This game uses these sounds from freesound:                          |");
	mvprintw(6, 0, "| Blood-Curdling Scream - Male by grghson:                             |");
	mvprintw(7, 0, "|  https://freesound.org/people/grghson/                               |");
	mvprintw(8, 0, "| Door, Front, Opening, A by InspectorJ:                               |");
	mvprintw(9, 0, "|  https://freesound.org/people/InspectorJ/                            |");
	mvprintw(10, 0,"| going_up_stairs.wav by soulbaby71:                                   |");
	mvprintw(11, 0,"|  https://freesound.org/people/soulbaby71/                            |");
	mvprintw(12, 0,"|----------------------------------------------------------------------|");
	mvprintw(13, 0,"| Virtutes Instrumenti by Kevin MacLeod                                |");
	mvprintw(14, 0,"| Link: https://incompetech.filmmusic.io/song/4590-virtutes-instrumenti|");
	mvprintw(15, 0,"| License: http://creativecommons.org/licenses/by/4.0/                 |");
	mvprintw(16, 0,"|----------------------------------------------------------------------|");
	mvprintw(17, 0,"| Plans in Motion by Kevin MacLeod                                     |");
	mvprintw(18, 0,"| Link: https://incompetech.filmmusic.io/song/4225-plans-in-motion     |");
	mvprintw(19, 0,"| License: http://creativecommons.org/licenses/by/4.0/                 |");
	mvprintw(20, 0,"|----------------------------------------------------------------------|");
	mvprintw(21, 0,"| Press any key to go back to title screen.                            |");
	mvprintw(22, 0,"+======================================================================+");
	nodelay(stdscr, FALSE);
	refresh();
	getch();
	nodelay(stdscr, TRUE);
	return;
}

void title_glossary(){
	clear();
	mvprintw(0, 0, "+======================================================================+");
	mvprintw(1, 0, "|                              Glossary:                               |");
	mvprintw(2, 0, "|                                                                      |");
	mvprintw(3, 0, "|                                                                      |");
	mvprintw(4, 0, "|                                                                      |");
	mvprintw(5, 0, "|                                                                      |");
	mvprintw(6, 0, "|                                                                      |");
	mvprintw(7, 0, "|                                                                      |");
	mvprintw(8, 0, "|                                                                      |");
	mvprintw(9, 0, "|                                                                      |");
	mvprintw(10, 0,"|                                                                      |");
	mvprintw(11, 0,"|                  Press any key to go back to menu.                   |");
	mvprintw(12, 0,"+======================================================================+");
	//Snake
	attron(COLOR_PAIR(ENEMY_COLOR));
	attron(A_BOLD);
		mvprintw(2, 2, "S");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(ENEMY_COLOR));
	mvprintw(2, 3, " - Snake");
	//Key
	attron(COLOR_PAIR(KEY_COLOR));
	attron(A_BOLD);
		mvprintw(3, 2, "F");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(KEY_COLOR));
	mvprintw(3, 3, " - Key");
	//Door
	attron(COLOR_PAIR(DOOR_COLOR));
	attron(A_BOLD);
		mvprintw(4, 2, " ");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(DOOR_COLOR));
	mvprintw(4, 3, " - Door");
	//Spider
	attron(COLOR_PAIR(ENEMY_COLOR));
		mvprintw(5, 2, "M");
	attroff(COLOR_PAIR(ENEMY_COLOR));
	mvprintw(5, 3, " - Spider");
	//Lava
	attron(COLOR_PAIR(LAVA_COLOR));
		mvprintw(6, 2, "~");
	attroff(COLOR_PAIR(LAVA_COLOR));
	mvprintw(6, 3, " - Lava");
	//Warrior
	attron(COLOR_PAIR(ENEMY_COLOR));
	attron(A_BOLD);
		mvprintw(7, 2, "O");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(ENEMY_COLOR));
	mvprintw(7, 3, " - Warrior");
	//Checkpoint
	attron(COLOR_PAIR(CHECKPOINT_NO_PLAYER_COLOR));
	attron(A_BOLD);
		mvprintw(8, 2, " ");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(CHECKPOINT_NO_PLAYER_COLOR));
	mvprintw(8, 3, " - Checkpoint");
	//Stairs
	attron(COLOR_PAIR(STAIRS_COLOR));
		mvprintw(9, 2, "@");
	attroff(COLOR_PAIR(STAIRS_COLOR));
	mvprintw(9, 3, " - Stairs");
	//Player
	attron(COLOR_PAIR(PLAYER_COLOR));
		mvprintw(10, 2, "O");
	attroff(COLOR_PAIR(PLAYER_COLOR));
	mvprintw(10, 3, " - Player");
	//End of Glossary
	nodelay(stdscr, FALSE);
	getch();
	nodelay(stdscr, TRUE);
	clear();
	return;
}
