/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//Pre-processor
#ifndef DUNGEON_H
#define DUNGEON_H

#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"
#include "unistd.h"
#include "raylib.h"
#include "curses.h"
#include "time.h"
#include "stdint.h"
#include "string.h"

//Define colors
#define PLAYER_COLOR 1
#define WALL_COLOR 2
#define KEY_COLOR 3
#define DOOR_COLOR 4
#define LAVA_COLOR 5
#define ENEMY_COLOR 6
#define CHECKPOINT_NO_PLAYER_COLOR 7
#define CHECKPOINT_PLAYER_COLOR 13
#define STAIRS_COLOR 8
#define SPACE_COLOR 9
#define WATER_COLOR 10
#define PUSH_COLOR 11
#define SWITCH_COLOR 12
#define DIALOGUE_NAME_COLOR 14
//Define Keyboard Buttons
#define KEY_RETURN 10
#ifdef DEBUG_MODE
extern int toggle_dungeon_walls;
extern int toggle_time;
extern int toggle_godmode;
#endif
/*-----------------------------------------------------*/
//Functions
//Gameplay Related Functions
	void end_game();
	void begin_user_input();
	void end_user_input();
	void tick_clock();
	void stats();
	void term_nagScreen();
//Title Screen Functions
	void title_screen();
	void title_choose_level();
	void title_credits();
	void title_glossary();
//Sound Related Functions
	void sound_key_get();
	void sound_door_open();
	void sound_player_died();
	void sound_next_level();
	void sound_door_locked();
	void sound_checkpoint_reached();
//Level Functions
	void level_1();
	void level2();
	void level3();
//Core Functions
	void dungeonGen( const int8_t dl[], int8_t *pd );
	void playerMovement();
	void playerDraw();
	void dialogue( const char dialogueText[], const char whoIsSpeaking[] );
//Enemy Functions
	void fireball_withPattern( char direction, int8_t *location, int8_t startingLocation[], int8_t endLocation[], int8_t speed, int8_t *pattern, int8_t *playerDead );
	void fireball( int8_t start[2], char direction, int8_t end[2], int8_t *currentLocation, int8_t speed, int8_t *playerDead );
	void snake( char movementInstructions[], int8_t *instruction, int8_t *location, int8_t speed, int8_t *goingBack, int8_t looping, int8_t *playerDead );
	void warrior( int8_t *activated, int8_t *currentLocation, const int8_t dungeonLayout[], int8_t *dead, int8_t roomDimensions[], int8_t speed, int8_t startingLocation[], int8_t goBack, int8_t *playerDead );
	void raceSpider( char movementInstructions[], int8_t *instruction, int8_t *location, int8_t *activated, int8_t triggerLocation[], int8_t speed, int8_t *finished, int8_t *playerDead  );
	void bouncingBall( int8_t rd[], int8_t *hd, int8_t *vd, int8_t s, int8_t *l, int8_t *pd);
	void spider( int8_t *l, int8_t s, int8_t *a, int8_t rd[], int8_t *pd );
	void worm( int8_t (*location)[2], int8_t roomDimensions[4], char *direction, int8_t length, int8_t speed, const int8_t dungeonLayout[], int8_t *playerDead );
//Object Functions
	void pushableBlock( int8_t *ol, int8_t *nl, int8_t *dio, int8_t *d, const int8_t dl[], int8_t sl[], int8_t dlo[] );
	int blockCollide ( const int8_t dl[], int8_t bnl[] );
	void keydoor ( int8_t kl[], int8_t *kg, int8_t dl[], int8_t *dio );
	void checkpoints( int8_t *alreadyActivated, int8_t coordinates[], int8_t *playerCheckpoint );
/*-----------------------------------------------------*/
//Global Variables
	extern int64_t tick; //50 ticks equals 1 second.
	extern int8_t checkpoint[];
	extern uint16_t deaths;
	extern int8_t level;
	//Sounds
		Sound dooropening_sound;
		Sound doorlocked_sound;
		Sound checkpoint_sound;
		Sound stairs_sound;
		Sound playerdead_sound;
	//Player Related
		extern int8_t old_player_cooridinates[2]; //Where player was
		extern int8_t new_player_cooridinates[2]; //Where player is
	//Enemies
		struct Fireball {
			int8_t currentLocation[2]; //Where it is currently
			int8_t start[2]; //Where it starts
			int8_t end[2]; //Where it ends, once it reaches here, it goes back to start.
		};
		struct Fireball_withPattern {
			int8_t location[2];
			int8_t start[2];
			int8_t end[2];
			int8_t pattern;
		};
		struct Snake {
			char movementInstructions[200];
			int8_t goingBack;
			int8_t instruction;
			int8_t location[2];
		};
		struct Warrior {
			int8_t currentLocation[2]; //Coordinates in terminal
			int8_t dead; //Is it dead?
			int8_t activated; //Is it activated?
			int8_t room_dimensions[4]; //4 Coordinates, should be set in this order: 1. Top, 2. Right, 3. Bottom, 4. Left. The numbers to put in here are the respective coordinates of said side. So for Top, you would put it's y coordinate.
			int8_t start[2]; //Where should it start?
		};
		struct RacerSpider {
			char movementInstructions[200]; //Controls Movement. N is Up, E is right, S is down, W is left, and F is Finished. F should always be the last instruction.
			int8_t instruction; //Determines how far into movementInstructions it is.
			int8_t location[2]; //Where it is currently.
			int8_t activated; //Determines if it's activated or not.
			int8_t triggerLocation[2]; //Location where the player needs to be for spider to activate.
			int8_t finished; //Once it has reached instruction "F" in movementInstructions, this variable tells the function raceSpider() that there are no more instructions.
		};
		struct BouncingBall {
			int8_t horizontalDirection; //0 is left, 1 is right.
			int8_t verticalDirection; // 0 is up, 1 is down.
			int8_t roomDimensions[4];
			int8_t location[2];
		};
		struct Spider {
			int8_t location[2];
			int8_t roomDimensions[4];
			int8_t activated;
		};
		struct Worm {
			char direction;
			int8_t location[100][2];
		};
	//Objects
		struct PushableBlock {
			int8_t blockOldLoc[2]; //Where the block was
			int8_t blockNewLoc[2]; //Where the block will be.
			int8_t doorIsOpened;
			int8_t destroyed;
			int8_t switchLocation[2];
			int8_t doorLocation[2];
		};

		struct Checkpoint {
			int8_t alreadyActivated; //Variable that says if checkpoint was already activated or not.
			int8_t location[2]; //Location of checkpoint
		};

		struct Keydoor {
			int8_t keyLocation[2];
			int8_t keyGot;
			int8_t doorLocation[2];
			int8_t doorIsOpened;
		};


#endif
