/* 
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/
#include "../dungeon.h"

//             Key          Key Got     Door         Door is
//             Location                 Location     Opened
void keydoor ( int8_t kl[], int8_t *kg, int8_t dl[], int8_t *dio ) {
	mvaddch( kl[0], kl[1], ' ' );
	mvaddch( dl[0], dl[1], ' ' );
	if ( *kg != 1 ) {
		
		attron( COLOR_PAIR(KEY_COLOR) );
		attron( A_BOLD );
			mvaddch( kl[0], kl[1], 'F' );
		attroff( A_BOLD );
		attroff( COLOR_PAIR(KEY_COLOR) );
		
		if ( old_player_cooridinates[0] == kl[0] && old_player_cooridinates[1] == kl[1] ) {
			*kg = 1;
		}
	}

	if ( *dio != 1 ) {
		
		attron( COLOR_PAIR(DOOR_COLOR) );
			mvaddch( dl[0], dl[1], ' ' );
		attroff( COLOR_PAIR(DOOR_COLOR) );

		if ( new_player_cooridinates[0] == dl[0] && new_player_cooridinates[1] == dl[1] ) {
			
			switch ( *kg ) {
				case 0:
					memcpy( new_player_cooridinates, old_player_cooridinates, sizeof(old_player_cooridinates) );
					break;
				case 1:
					*dio = 1;
					break;
				default:
					printf("E4: Unable to figure out if player has key or not.");
					break;
			}
		}
	}
	return;
}

/*                  Old Locat.  New Locat,  Door is      Destroyed  Dungeon          Switch       Door*/
/*                                          Opened                  Layout           Location     Location*/
void pushableBlock( int8_t *ol, int8_t *nl, int8_t *dio, int8_t *d, const int8_t dl[], int8_t sl[], int8_t dlo[] ){
	int a = 0;
	mvaddch( ol[0], ol[1], ' ' );
	memcpy(ol, nl, sizeof(int8_t)*2 );
	
	if ( new_player_cooridinates[0] == ol[0] && new_player_cooridinates[1] == ol[1] ) {
		
		if ( old_player_cooridinates[0] < ol[0] ) { //Above
			nl[0]++;
		}
		else if ( old_player_cooridinates[1] > ol[1] ) { //To the Right
			nl[1]--;
		}
		else if ( old_player_cooridinates[0] > ol[0] ) { //Bottom
			nl[0]--;
		}
		else if ( old_player_cooridinates[1] < ol[1] ) { //To the Left
			nl[1]++;
		}

		a = blockCollide( dl, nl );

		if ( a == 1 ) { //Collided with wall.
			memcpy(nl, ol, sizeof(int8_t)*2 );
			memcpy(new_player_cooridinates, old_player_cooridinates, sizeof(int8_t)*2 );
		}
		else if ( a == 2 ) { //Collided with Lava
			*d = 1;
		}

	}

	//Door
	if ( ol[0] == sl[0] && ol[1] == sl[1] ) {
		*dio = 1;
	}
	if ( new_player_cooridinates[0] == dlo[0] && new_player_cooridinates[1] == dlo[1] && *dio != 1 ) {
		memcpy(new_player_cooridinates, old_player_cooridinates, sizeof(int8_t)*2 );
	}

	attron( COLOR_PAIR(SWITCH_COLOR) );
		mvaddch( sl[0], sl[1], ' ' );
	attroff( COLOR_PAIR(SWITCH_COLOR) );

	switch ( *dio ) {
		case 0:
			attron( COLOR_PAIR(DOOR_COLOR) );
				mvaddch( dlo[0], dlo[1], ' ');
			attroff( COLOR_PAIR(DOOR_COLOR) );
			break;
		case 1:
			mvaddch( dlo[0], dlo[1], ' ' );
			break;
		default:
			printf("E1: Pushable block door broken. Variable is not 0 nor 1.");
			break;
	}

	if ( *d != 1 ) {
		attron( COLOR_PAIR(PUSH_COLOR) );
		attron( A_BOLD );
			mvaddch( ol[0], ol[1], ' ');
		attroff( A_BOLD );
		attroff( COLOR_PAIR(PUSH_COLOR) );
	}
	return;
}

/*                  Dungeon            Block New     */
/*                  Layout             Location      */
int blockCollide ( const int8_t dl[], int8_t bnl[] ) {
	int a = 0; // Block Number
	int b[2]; // Location
	memcpy( b, (int[]){0, 0}, sizeof(int)*2 );
	for ( a = 0; a != 1000; a++ ) {

		if ( a % 40 == 0 && b[1] != 0 ){
			b[0]++;
			b[1] = 0;
		}
		else {
			b[1]++;
		}

		if ( b[0] == bnl[0] && b[1] == bnl[1] && dl[a] == 0 ) {
			return 1;
		}
		else if ( b[0] == bnl[0] && b[1] == bnl[1] && dl[a] == 3 ) {
			return 1;
		}
		else if ( b[0] == bnl[0] && b[1] == bnl[1] && dl[a] == 2 ) {
			return 2;
		}
		
	}
	return 0;
}