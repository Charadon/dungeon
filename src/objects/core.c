#include "../dungeon.h"

void dungeonGen( const int8_t dl[], int8_t *pd ) {
	int a; //Block
	int y = 0; //Row
	int x = -1; //Column
	for ( a = 0; a != 1000; a++ ) { //Drawer loop

		if ( a % 40 == 0 && x != -1 ) { //Checks if the drawer loop needs to go down a row.
			y++;
			x = 0;
		} else {
			x++;
		}

		switch ( dl[a] ) { //Detecting what to draw
			case 0: //Walls
				attron( COLOR_PAIR(WALL_COLOR) );
					mvaddch( y, x, ' ');
				attroff( COLOR_PAIR(WALL_COLOR) );
				break;
			case 1: //Space
				break;
			case 2: //Lava
				attron( COLOR_PAIR(LAVA_COLOR) );
					mvaddch( y, x, '~');
				attroff( COLOR_PAIR(LAVA_COLOR) );
				break;
			case 3: //Legacy
				break;
			case 4:
				break;
			default:
				break;
		}

		//Collision Detection
		#ifdef DEBUG_MODE
			if ( toggle_dungeon_walls == 0 ) {
				if ( new_player_cooridinates[0] == y && new_player_cooridinates[1] == x && dl[a] == 0 )
					memcpy( new_player_cooridinates, old_player_cooridinates, sizeof(int8_t)*2 );
				else if ( new_player_cooridinates[0] == y && new_player_cooridinates[1] == x && dl[a] == 2 )
					*pd = 1;
			}
		#else
			if ( new_player_cooridinates[0] == y && new_player_cooridinates[1] == x && dl[a] == 0 )
				memcpy( new_player_cooridinates, old_player_cooridinates, sizeof(int8_t)*2 );
			else if ( new_player_cooridinates[0] == y && new_player_cooridinates[1] == x && dl[a] == 2 )
				*pd = 1;
		#endif
	}
	return;
}

#ifdef DEBUG_MODE
void debugMode( int keypress ) {
	switch ( keypress ) {
		case '?':
			if ( toggle_dungeon_walls == 1 )
				toggle_dungeon_walls = 0;
			else
				toggle_dungeon_walls = 1;
			wclear( stdscr );
			break;
		case '!':
			if ( toggle_time == 1 )
				toggle_time = 0;
			else
				toggle_time = 1;
			wclear( stdscr );
			break;
		case '+':
			if ( toggle_godmode == 1 )
				toggle_godmode = 0;
			else
				toggle_godmode = 1;
			wclear( stdscr );
			break;
		default:
			break;
	}
	if ( toggle_time == 1 )
		tick = -1;
	wattron( stdscr, COLOR_PAIR(STAIRS_COLOR) );
		mvwprintw( stdscr, 29, 0, "DEBUG MODE: ON" );
		mvwprintw( stdscr, 30, 0, "Wall Collision Off: %i ?", toggle_dungeon_walls );
		mvwprintw( stdscr, 31, 0, "Time Stopped: %i !", toggle_time );
		mvwprintw( stdscr, 32, 0, "God Mode: %i +", toggle_godmode );
	wattroff( stdscr, COLOR_PAIR(STAIRS_COLOR) );
}
#endif

void playerMovement() {
	int keypress = getch();	
	#ifdef DEBUG_MODE
	debugMode( keypress );
	#endif
	memcpy( old_player_cooridinates, new_player_cooridinates, sizeof(int8_t)*2 );
	switch( toupper( keypress ) ) {
		//WASD
		case 'W':
			new_player_cooridinates[0]--;
			break;
		case 'S':
			new_player_cooridinates[0]++;
			break;
		case 'A':
			new_player_cooridinates[1]--;
			break;
		case 'D':
			new_player_cooridinates[1]++;
			break;
		//Arrow Keys
		case KEY_UP:
			new_player_cooridinates[0]--;
			break;
		case KEY_DOWN:
			new_player_cooridinates[0]++;
			break;
		case KEY_LEFT:
			new_player_cooridinates[1]--;
			break;
		case KEY_RIGHT:
			new_player_cooridinates[1]++;
			break;
		//Vim Keys
		case 'H':
			new_player_cooridinates[1]--;
			break;
		case 'J':
			new_player_cooridinates[0]++;
			break;
		case 'K':
			new_player_cooridinates[0]--;
			break;
		case 'L':
			new_player_cooridinates[1]++;
			break;
		case 'Q':
			end_game(); //Debug
			break;
		default:
			break;
	}

	return;
}

void playerDraw() {
	mvaddch(old_player_cooridinates[0], old_player_cooridinates[1], ' '); //Removes player's symbol from previous location.
	attron(COLOR_PAIR(PLAYER_COLOR));
	attron( A_BOLD );
		mvaddch(new_player_cooridinates[0], new_player_cooridinates[1], 'O'); //Draws the Player
	attroff( A_BOLD );
	attroff(COLOR_PAIR(PLAYER_COLOR));
}

void checkpoints( int8_t *alreadyActivated, int8_t coordinates[], int8_t *playerCheckpoint ) {
	if ( old_player_cooridinates[0] == coordinates[0] && old_player_cooridinates[1] == coordinates[1] ) {
		switch ( *alreadyActivated ) {
			case 0:
				*alreadyActivated = 1;
				memcpy( playerCheckpoint, coordinates, sizeof(int8_t)*2 );
				attron( COLOR_PAIR(CHECKPOINT_PLAYER_COLOR) );
				attron( A_BOLD );
					mvwaddch( stdscr, coordinates[0], coordinates[1], 'O' );
				attroff( A_BOLD );
				attroff( COLOR_PAIR(CHECKPOINT_PLAYER_COLOR) );
				break;
			case 1:
				attron( COLOR_PAIR(CHECKPOINT_PLAYER_COLOR) );
				attron( A_BOLD );
					mvwaddch( stdscr, coordinates[0], coordinates[1], 'O' );
				attroff( A_BOLD );
				attroff( COLOR_PAIR(CHECKPOINT_PLAYER_COLOR) );
				break;
			default:
				fprintf( stderr, "ERROR: CHECKPOINT FAILED TO GET VALUE OF alreadyActivated IN LEVEL %i", level );
				break;
		}
	} else {	
		attron( COLOR_PAIR(CHECKPOINT_NO_PLAYER_COLOR) );
			mvaddch( coordinates[0], coordinates[1], ' ' );
		attroff( COLOR_PAIR(CHECKPOINT_NO_PLAYER_COLOR) );
	}

	return;
}

void term_nagScreen() {
	int y, x;
	getmaxyx( stdscr, y, x );
	if ( y < 35 || x < 65 ) {
		clear();
		while( y < 35 || x < 65 ) {
			mvprintw( 0, 0, "Hey asshat, resize your terminal to atleast 30 lines by 60 columns. You asshole. -The Developer\nLINES: %i, COLUMNS: %i", y, x );
			refresh();
			getmaxyx( stdscr, y, x );
		}
		clear();
	}
	return;
}

void dialogue( const char dialogueText[], const char whoIsSpeaking[] ) {
	int a; //Which letter is being rendered
	int b = 0; //How far along.
	int line = 0; //Line, max 3.
	int length = strlen( dialogueText );
	attron( COLOR_PAIR( DIALOGUE_NAME_COLOR ) );
		mvwprintw( stdscr, 25, 0, "%s:", whoIsSpeaking );
	attroff( COLOR_PAIR( DIALOGUE_NAME_COLOR ) );
	attron( A_BOLD );
	for ( a = 0;  a < length; a++, b++ ){
		usleep(10000);
		if ( dialogueText[a] == '\n' ){
			line++;
			b = -1;
		}
		mvwaddch( stdscr, 26 + line, b, dialogueText[a] );
		refresh();
	}
	attroff( A_BOLD );
	refresh();
	nodelay( stdscr, FALSE );
	getch();
	nodelay( stdscr, TRUE );
	wclear( stdscr );
	return;
}
