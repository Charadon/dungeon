/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../dungeon.h"

/*							Direction Location   Start       End         Speed     Pattern    Old_Pattern*/
void fireball_withPattern( char direction, int8_t *location, int8_t startingLocation[], int8_t endLocation[], int8_t speed, int8_t *pattern, int8_t *playerDead ) {
	attron( COLOR_PAIR(ENEMY_COLOR) );
	if (*pattern == -1) {
		int8_t oldPattern = *pattern;
		while(*pattern == oldPattern)
			*pattern = rand() % 3;
	}
	if (tick % speed == 0) {
		if ( direction == 'N' || direction == 'S' ) { //Cleaning up previous location
			mvaddch(location[0], location[1] - 1, ' ');
			mvaddch(location[0], location[1], ' ');
			mvaddch(location[0], location[1] + 1, ' ');
		} else {
			mvaddch(location[0] - 1, location[1], ' ');
			mvaddch(location[0], location[1], ' ');
			mvaddch(location[0] + 1, location[1], ' ');
		}
		switch ( direction ) { //Movement
			case 'N':
				location[0]--;
				break;
			case 'E':
				location[1]++;
				break;
			case 'S':
				location[0]++;
				break;
			case 'W':
				location[1]--;
				break;
			default:
				fprintf( stderr, "ERROR: FAILED TO GET DIRECTION FOR FIREBALL"
					  "IN LEVEL %i", level );
				break;
		}
		if ( direction == 'N' || direction == 'S' ) { //Drawing
			switch (*pattern){
				case 0: // * x *
					mvaddch(location[0], location[1] - 1, '*');
					mvaddch(location[0], location[1], ' ');
					mvaddch(location[0], location[1] + 1, '*');
					break;
				case 1: // x * *
					mvaddch(location[0], location[1] - 1, ' ');
					mvaddch(location[0], location[1], '*');
					mvaddch(location[0], location[1] + 1, '*');
					break;
				case 2: // * * x
					mvaddch(location[0], location[1] - 1, '*');
					mvaddch(location[0], location[1], '*');
					mvaddch(location[0], location[1] + 1, ' ');
					break;
				default:
					printf("ERROR: Printing fireballs failed while in level %d", level);
					break;
			}
		} else {
			switch (*pattern){
				case 0: // * x *
					mvaddch(location[0] - 1, location[1], '*');
					mvaddch(location[0], location[1], ' ');
					mvaddch(location[0] + 1, location[1], '*');
					break;
				case 1: // x * *
					mvaddch(location[0] - 1, location[1], ' ');
					mvaddch(location[0], location[1], '*');
					mvaddch(location[0] + 1, location[1], '*');
					break;
				case 2: // * * x
					mvaddch(location[0] - 1, location[1], '*');
					mvaddch(location[0], location[1], '*');
					mvaddch(location[0] + 1, location[1], ' ');
					break;
				default:
					printf("E2: Printing fireballs failed while in level %d", level);
					break;
			}
		}
	}
	//Collision Detection
	if ( direction == 0 || direction == 2 ) {
		switch (*pattern) {
			case 0: // * x *
				if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1] - 1 ) // o - -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1] + 1 ) // - - o
					*playerDead = 1;
				break;
			case 1: // x * *
				if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1]) // - o -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1] + 1 ) // - - o
					*playerDead = 1;
				break;
			case 2: // * * x
				if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1] - 1 )  // o - -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1])  // - o -
					*playerDead = 1;
				break;
			default:
				break;
		}
	} else {
		switch (*pattern) {
			case 0: // * x *
				if (new_player_cooridinates[0] == location[0] - 1 && new_player_cooridinates[1] == location[1] )  // o - -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] + 1 && new_player_cooridinates[1] == location[1] )  // - - o
					*playerDead = 1;
				break;
			case 1: // x * *
				if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1])  // - o -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] + 1 && new_player_cooridinates[1] == location[1] )  // - - o
					*playerDead = 1;
				break;
			case 2: // * * x
				if (new_player_cooridinates[0] == location[0] - 1 && new_player_cooridinates[1] == location[1] )  // o - -
					*playerDead = 1;
				else if (new_player_cooridinates[0] == location[0] && new_player_cooridinates[1] == location[1])  // - o -
					*playerDead = 1;
				break;
			default:
				break;
		}
	}
	if (location[0] == endLocation[0] && location[1] == endLocation[1] ){ //Detects if fireball has reached it's end of range.
		memcpy(location, startingLocation, sizeof(int8_t)*2 );
		*pattern = -1;
	}
	attroff( COLOR_PAIR(ENEMY_COLOR) );
	return;
}

void fireball( int8_t start[2], char direction, int8_t end[2], int8_t *currentLocation, int8_t speed, int8_t *playerDead ) {
	mvaddch(currentLocation[0], currentLocation[1], ' ');
	if ( tick % speed == 0 ) {
		switch ( toupper( direction ) ){
			case 'N': //Up
				currentLocation[0]--;
				break;
			case 'E': //Right
				currentLocation[1]++;
				break;
			case 'S': //Down
				currentLocation[0]++;
				break;
			case 'W': //Left
				currentLocation[1]--;
				break;
			default:
				fprintf(stderr, "ERROR: COULDN'T FIGURE OUT FIREBALL DIRECTION.");
				break;
		}
		if (currentLocation[0] == end[0] && currentLocation[1] == end[1])
			memcpy(currentLocation, start, sizeof(int8_t)*2 );
	}
	if(old_player_cooridinates[0] == currentLocation[0] && old_player_cooridinates[1] == currentLocation[1])
		*playerDead = 1;
	attron(A_BOLD);
	attron(COLOR_PAIR(ENEMY_COLOR));
		mvwaddch( stdscr, currentLocation[0], currentLocation[1], '*' );
	attroff(COLOR_PAIR(ENEMY_COLOR));
	attroff(A_BOLD);
	return;
}

void snake( char movementInstructions[], int8_t *instruction, int8_t *location, int8_t speed, int8_t *goingBack, int8_t looping, int8_t *playerDead ) {
	mvaddch(location[0], location[1], ' ');
	if ( tick % speed == 0 ) {
		if ( *goingBack == 0 )
			MoveAgain:
			switch( toupper(movementInstructions[*instruction]) ) {
				case 'N':
					location[0]--;
					(*instruction)++;
					break;
				case 'E':
					location[1]++;
					(*instruction)++;
					break;
				case 'S':
					location[0]++;
					(*instruction)++;
					break;
				case 'W':
					location[1]--;
					(*instruction)++;
					break;
				default:
					if ( looping == 1 ) {
						*goingBack = 1;
						(*instruction)--;
					}
					else {
						*instruction = 0;
						goto MoveAgain;
					}
					break;
			}
		else
			switch( toupper(movementInstructions[*instruction]) ) {
				case 'N':
					location[0]++;
					(*instruction)--;
					break;
				case 'E':
					location[1]--;
					(*instruction)--;
					break;
				case 'S':
					location[0]--;
					(*instruction)--;
					break;
				case 'W':
					location[1]++;
					(*instruction)--;
					break;
				default:
					*goingBack = 0;
					(*instruction)++;
					break;
			}

	}
	if ( old_player_cooridinates[0] == location[0] && old_player_cooridinates[1] == location[1] )
		*playerDead = 1;
	attron(A_BOLD);
	attron(COLOR_PAIR(ENEMY_COLOR));
		mvaddch(location[0], location[1], 'S');
	attroff(COLOR_PAIR(ENEMY_COLOR));
	attroff(A_BOLD);
	return;
}

#define NORTH_WEST 0
#define NORTH_EAST 1
#define SOUTH_EAST 2
#define SOUTH_WEST 3
#define NORTH 4
#define EAST 5
#define SOUTH 6
#define WEST 7

void warrior( int8_t *activated, int8_t *currentLocation, const int8_t dungeonLayout[], int8_t *dead, int8_t roomDimensions[], int8_t speed, int8_t startingLocation[], int8_t goBack, int8_t *playerDead ) {
	int8_t oldLocation[2];
	mvaddch( currentLocation[0], currentLocation[1], ' ' );
	memcpy( oldLocation, currentLocation, sizeof(int8_t)*2 );
	if ( tick % speed == 0 && *dead == 0 ){
		if ( new_player_cooridinates[0] > roomDimensions[0] && new_player_cooridinates[1] < roomDimensions[1] && new_player_cooridinates[0] < roomDimensions[2] && new_player_cooridinates[1] > roomDimensions[3] ) {
			*activated = 1;
		}
		else {
			*activated = 0;
		}
		int CannotGoUp = 0, CannotGoRight = 0, CannotGoDown = 0, CannotGoLeft = 0, CannotGoUpLeft = 0, 
			CannotGoUpRight = 0, CannotGoDownRight = 0, CannotGoDownLeft = 0;
		int LastMovement = 0;
		switch ( *activated ) {
			//Checks if player is in the room.
			case 0:
				if ( goBack == 1 ) {
					//THESE NEED TO BE UPDATED
					if ( startingLocation[0] < currentLocation[0] ) //Going Up
						currentLocation[0]--;
					if ( startingLocation[1] > currentLocation[1] ) //Going Right
						currentLocation[1]++;
					if ( startingLocation[0] > currentLocation[0] ) //Going Down
						currentLocation[0]++;
					if ( startingLocation[1] < currentLocation[1] ) //Going Left
						currentLocation[1]--;
				}
				break;
			case 1:
				RecalculateMovement:
				if ( new_player_cooridinates[0] < currentLocation[0] && new_player_cooridinates[1] < currentLocation[1] && CannotGoUpLeft == 0 ) { //Up-Left
					currentLocation[0]--;
					currentLocation[1]--;
					LastMovement = NORTH_WEST;
				}
				else if ( new_player_cooridinates[0] < currentLocation[0] && new_player_cooridinates[1] > currentLocation[1] && CannotGoUpRight == 0 ) { //Up-Right
					currentLocation[0]--;
					currentLocation[1]++;
					LastMovement = NORTH_EAST;
				}
				else if ( new_player_cooridinates[0] > currentLocation[0] && new_player_cooridinates[1] > currentLocation[1] && CannotGoDownRight == 0 ) { //Down-Right
					currentLocation[0]++;
					currentLocation[1]++;
					LastMovement = SOUTH_EAST;
				}
				else if ( new_player_cooridinates[0] > currentLocation[0] && new_player_cooridinates[1] < currentLocation[1] && CannotGoDownLeft == 0 ) { //Down-Left
					currentLocation[0]++;
					currentLocation[1]--;
					LastMovement = SOUTH_WEST;
				}
				else if ( new_player_cooridinates[0] < currentLocation[0] && CannotGoUp == 0 ) { //Going Up
					currentLocation[0]--;
					LastMovement = NORTH;
				}
				else if ( new_player_cooridinates[1] > currentLocation[1] && CannotGoRight == 0 ) { //Going Right
					currentLocation[1]++;
					LastMovement = EAST;
				}
				else if ( new_player_cooridinates[0] > currentLocation[0] && CannotGoDown == 0 ) { //Going Down
					currentLocation[0]++;
					LastMovement = SOUTH;
				}
				else if ( new_player_cooridinates[1] < currentLocation[1] && CannotGoLeft == 0 ) {	//Going Left
					currentLocation[1]--;
					LastMovement = WEST;
				}
				break;
			default:
				printf("Warrior Function broken at switch statement for activation variable.");
				end_game();
		}
		int blockNumber;
		int blockLocation[2] = {0, -1};
		for ( blockNumber = 0; blockNumber != 1000; blockNumber++ ) { //Dynamic block collision detector

			if ( blockNumber % 40 == 0 && blockLocation[1] != -1 ){ //When it reaches end of line (40), it begins to scan on the next line.
				blockLocation[0]++;
				blockLocation[1] = 0;
			}
			else{
				blockLocation[1]++;
			}

			if ( dungeonLayout[blockNumber] == 0 && blockLocation[0] == currentLocation[0] && blockLocation[1] == currentLocation[1] ) {
				memcpy( currentLocation, oldLocation, sizeof(int8_t)*2 );
				switch ( LastMovement ) {
					case NORTH_WEST:
						CannotGoUpLeft = 1;
						break;
					case NORTH_EAST:
						CannotGoUpRight = 1;
						break;
					case SOUTH_EAST:
						CannotGoDownRight = 1;
						break;
					case SOUTH_WEST:
						CannotGoDownLeft = 1;
						break;
					case NORTH:
						CannotGoUp = 1;
						break;
					case EAST:
						CannotGoRight = 1;
						break;
					case SOUTH:
						CannotGoDown = 1;
						break;
					case WEST:
						CannotGoLeft = 1;
						break;
					default:
						fprintf( stderr, "ERROR: LastMovement is invalid for warrior in level %i", level );
						break;
				}
				goto RecalculateMovement;
				break; //Got our result, time to leave.
			}
			else if ( dungeonLayout[blockNumber] == 2 && blockLocation[0] == currentLocation[0] && blockLocation[1] == currentLocation[1] ) {
				*dead = 1;
				break; //Got our result, time to leave.
			}

		}

	}
	if ( currentLocation[0] == new_player_cooridinates[0] && currentLocation[1] == new_player_cooridinates[1] ) //Has killed player?
			*playerDead = 1;
	attron( COLOR_PAIR(ENEMY_COLOR) );
	attron( A_BOLD );
		mvaddch( currentLocation[0], currentLocation[1], 'O' );
	attroff( COLOR_PAIR(ENEMY_COLOR) );
	attroff( A_BOLD );
	return;
}


void raceSpider( char movementInstructions[], int8_t *instruction, int8_t *location, int8_t *activated, int8_t triggerLocation[], int8_t speed, int8_t *finished, int8_t *playerDead ) {
	mvaddch( location[0], location[1], ' ' );
	if ( old_player_cooridinates[0] == triggerLocation[0] && old_player_cooridinates[1] == triggerLocation[1] && *finished != 1 )
		*activated = 1;

	if ( tick % speed == 0 && *activated == 1 && *finished != 1 ) {
		switch ( movementInstructions[*instruction] ) {
			case 'N':
				location[0]--;
				break;
			case 'E':
				location[1]++;
				break;
			case 'S':
				location[0]++;
				break;
			case 'W':
				location[1]--;
				break;
			case 'F':
				*finished = 1;
				break;
			default:
				fprintf( stderr, "ERROR OCCURED: FAILED TO GET INSTRUCTION FOR RACER SPIDER IN LEVEL %i", level );
				break;
		}
		(*instruction)++;
	}

	if ( old_player_cooridinates[0] == location[0] && old_player_cooridinates[1] == location[1] )
		*playerDead = 1;
	attron( COLOR_PAIR(ENEMY_COLOR) );
	attron( A_BOLD );
		mvaddch( location[0], location[1], 'M' );
	attroff( A_BOLD );
	attroff( COLOR_PAIR(ENEMY_COLOR) );
	return;
}

/*           Location   Speed     Activated  Room Dimen.  Dungeon Layout     Player Died */
void spider( int8_t *l, int8_t s, int8_t *a, int8_t rd[], int8_t *pd ) {
	mvaddch( l[0], l[1], ' ' );

	//Checks if player is in room
	if ( old_player_cooridinates[0] >= rd[0] && old_player_cooridinates[1] <= rd[1] && old_player_cooridinates[0] <= rd[2] && old_player_cooridinates[1] >= rd[3] ) {
		*a = 1;
	}
	else {
		*a = 0;
	}

	if ( tick % s == 0 && *a == 1 ) {
		int ol[2];
		memcpy( ol, l, sizeof(int8_t)*2 );
		int r;
		reroll:
		r = rand() % 8;
		switch( r ) {
			case 0: //Up
				l[0]--;
				break;
			case 1: //Up-Right
				l[0]--;
				l[1]++;
				break;
			case 2: //Right
				l[1]++;
				break;
			case 3: //Down-Right
				l[0]++;
				l[1]++;
				break;
			case 4: //Down
				l[0]++;
				break;
			case 5: //Down-Left
				l[0]++;
				l[1]--;
				break;
			case 6: //Left
				l[1]--;
				break;
			case 7: //Left-Up
				l[0]--;
				l[1]--;
				break;
			default:
				printf( "E4: Diceroll failed for spider in level %d", level );
				break;
		}

		//Checks if previous movement left room, if it did, then it moves the spider back and rerolls until it gets a non-invalid movement
		if ( l[0] <= rd[0] ) { //Top
			memcpy( l, ol, sizeof(int8_t)*2 );
			goto reroll;
		}
		else if ( l[1] >= rd[1] ) {
			memcpy( l, ol, sizeof(int8_t)*2 );
			goto reroll;
		}
		else if ( l[0] >= rd[2] ) {
			memcpy( l, ol, sizeof(int8_t)*2 );
			goto reroll;
		}
		else if ( l[1] <= rd[3] ) {
			memcpy( l, ol, sizeof(int8_t)*2 );
			goto reroll;
		}

	}
	if ( old_player_cooridinates[0] == l[0] && old_player_cooridinates[1] == l[1] )
		*pd = 1;
	attron( COLOR_PAIR(ENEMY_COLOR) );
	attron( A_BOLD );
		mvaddch( l[0], l[1], 'M' );
	attroff( COLOR_PAIR(ENEMY_COLOR) );
	attroff( A_BOLD );
	return;
}

/*                 Room         Horizontal  Vertical    Speed     Location   Player */
/*                 Dimensions   Direction   Direction                        Died   */
void bouncingBall( int8_t rd[], int8_t *hd, int8_t *vd, int8_t s, int8_t *l, int8_t *pd) {
	mvaddch( l[0], l[1], ' ' );
	if ( tick % s == 0 ) {

		switch ( *hd ) {
			case 0: //Left
				l[1]--;
				break;
			case 1: //Right
				l[1]++;
				break;
			default:
				break;
		}

		switch ( *vd ) {
			case 0: //Up
				l[0]--;
				break;
			case 1: //Down
				l[0]++;
				break;
			default:
				break;
		}

		//Edge Detection
		if ( l[0] == rd[0] + 1 && l[1] != rd[1] - 1 && l[1] != rd[3] + 1)
			*vd = 1;
		else if ( l[1] == rd[1] - 1 )
			*hd = 0;
		else if ( l[0] == rd[2] - 1 && l[1] != rd[1] - 1 && l[1] != rd[3] + 1)
			*vd = 0;
		else if ( l[1] == rd[3] + 1 )
			*hd = 1;

		//Corner Detection
		if ( l[0] == rd[0] + 1 && l[1] == rd[3] + 1 ) {
			*vd = 1;
			*hd = 1;
		}
		else if ( l[0] == rd[0] + 1 && l[1] == rd[1] - 1 ) {
			*vd = 1;
			*hd = 0;
		}
		else if ( l[0] == rd[2] - 1 && l[1] == rd[3] + 1 ) {
			*vd = 0;
			*hd = 1;
		}
		else if ( l[0] == rd[2] - 1 && l[1] == rd[1] - 1 ) {
			*vd = 0;
			*hd = 0;
		}

	}

	if ( old_player_cooridinates[0] == l[0] && old_player_cooridinates[1] == l[1] )
		*pd = 1;

	attron( COLOR_PAIR(ENEMY_COLOR) );
	attron( A_BOLD );
		mvaddch( l[0], l[1], '0' );
	attroff( A_BOLD );
	attroff( COLOR_PAIR(ENEMY_COLOR) );
	return;
}

void worm( int8_t (*location)[2], int8_t roomDimensions[4], char *direction, int8_t length, int8_t speed, const int8_t dungeonLayout[], int8_t *playerDead ) {
	int bodyPart;
	int block;
	int blockLocation[2] = {0, -1};

	if ( tick % speed == 0 ) {
		for ( block = 0; block < 1000; block++ ) { //Cleanup old parts

			if ( block % 40 == 0 && blockLocation[1] != -1 ) {
				blockLocation[1] = 0;
				blockLocation[0]++;
			}
			else {
				blockLocation[1]++;
			}

			if ( dungeonLayout[block] == 4 && blockLocation[0] >= roomDimensions[0] && blockLocation[1] <= roomDimensions[1] && blockLocation[0] <= roomDimensions[2] && blockLocation[1] >= roomDimensions[3] )
				mvwaddch( stdscr, blockLocation[0], blockLocation[1], ' ' );

		}
		for ( bodyPart = length; bodyPart >= 1; bodyPart-- ) { //Shift array to the right
				location[bodyPart][0] = location[bodyPart - 1][0];
				location[bodyPart][1] = location[bodyPart - 1][1];
		}

		//Room bounds detection
		if ( location[0][0] == roomDimensions[0] && location[0][1] == roomDimensions[1] ) //Top Right Corner
			*direction = 'W';
		else if ( location[0][0] == roomDimensions[0] && location[0][1] == roomDimensions[3] ) //Top Left Corner
			*direction = 'S';
		else if ( location[0][0] == roomDimensions[2] && location[0][1] == roomDimensions[3] ) //Bottom Left Corner
			*direction = 'E';
		else if ( location[0][0] == roomDimensions[2] && location[0][1] == roomDimensions[1] ) //Bottom Right Corner
			*direction = 'N';

		switch ( *direction ) { //Movement
			case 'N':
				location[0][0]--;
				break;
			case 'E':
				location[0][1]++;
				break;
			case 'S':
				location[0][0]++;
				break;
			case 'W':
				location[0][1]--;
				break;
			default:
				break;
		}

	}
	wattron( stdscr, COLOR_PAIR(ENEMY_COLOR) );
	for ( bodyPart = 0; bodyPart <= length; bodyPart++ ) { //Drawing & Collision Detection

		if ( bodyPart % 2 == 0 )
			mvwaddch( stdscr, location[bodyPart][0], location[bodyPart][1], 'O' );
		else
			mvwaddch( stdscr, location[bodyPart][0], location[bodyPart][1], 'o' );

		if ( location[bodyPart][0] == new_player_cooridinates[0] && location[bodyPart][1] == new_player_cooridinates[1] )
			*playerDead = 1;

	}
	wattroff( stdscr, COLOR_PAIR(ENEMY_COLOR) );
	return;
}
