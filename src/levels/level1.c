/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../dungeon.h"

void level_1() {
	clear();
	//This variable determines the layout of the dungeon. 0 is walls, 1 is space, 2 is lava, 3 is checkpoints, 4 is clearing worms, 5 are stairs.
	const int8_t dungeonLayout[] = {
/* 00 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 01 */	0, 0, 1, 1, 1, 1, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 02 */	0, 0, 1, 1, 1, 1, 0, 4, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0,
/* 03 */	0, 0, 1, 1, 1, 1, 1, 4, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0,
/* 04 */	0, 0, 1, 1, 1, 1, 0, 4, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
/* 05 */	0, 0, 1, 1, 1, 1, 0, 4, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 2, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0,
/* 06 */	0, 0, 1, 1, 1, 1, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0, 0, 2, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0,
/* 07 */	0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0,
/* 08 */	0, 5, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,
/* 09 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0,
/* 10 */	0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0,
/* 11 */	0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0,
/* 12 */	0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0,
/* 13 */	0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0,
/* 14 */	0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0,
/* 15 */	0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
/* 16 */	0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0,
/* 17 */	0, 1, 0, 1, 2, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0,
/* 18 */	0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0,
/* 19 */	0, 1, 0, 1, 2, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0,
/* 20 */	0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0,
/* 21 */	0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0,
/* 22 */	0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0,
/* 23 */	0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0,
/* 24 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		/* 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 */
	//Sets player's initial location
	memcpy( old_player_cooridinates, (int8_t[]){23, 1}, sizeof(int8_t)*2 );
	memcpy( new_player_cooridinates, (int8_t[]){23, 1}, sizeof(int8_t)*2 );
	//Sets initial Checkpoint
	memcpy( checkpoint, (int8_t[]){23, 1}, sizeof(int8_t)*2 );
	int8_t levelGoing = 1;
	int8_t playerDead = 0;
	//Declare Variables
		struct Checkpoint chk1 = {
			0, {16, 7}
		};
		struct Checkpoint chk2 = {
			0, {14, 26}
		};
		struct Checkpoint chk3 = {
			0, {9, 37}
		};
		struct Keydoor kd1 = {
			{0}, 0, {0}, 0
		};
		struct Keydoor kd2 = {
			{0}, 0, {0}, 0
		};
		struct Keydoor kd3 = {
			{0}, 0, {0}, 0
		};
		struct Keydoor kd4 = {
			{0}, 0, {0}, 0
		};
		struct Keydoor kd5 = {
			{0}, 0, {0}, 0
		};
		struct Keydoor kd6 = {
			{0}, 0, {0}, 0
		};
		struct Snake s1 = {
			{'S', 'S'}, 0, 0, {17, 3}
		};
		struct Snake s2 = {
			{'N', 'N'}, 0, 0, {19, 5}
		};
		struct Snake s3 = {
			{'N', 'N'}, 0, 0, {23, 5}
		};
		struct Snake s4 = {
			{'S', 'S'}, 0, 0, {21, 4}
		};
		struct Snake s5 = {
			{'N', 'N', 'W', 'W', 'S', 'S', 'E', 'E'}, 0, 0, {15, 8}
		};
		struct Snake s6 = {
			{'E', 'E', 'S', 'S', 'W', 'W', 'N', 'N'}, 0, 0, {15, 9}
		};
		struct Snake s7 = {
			{'S', 'S', 'E', 'E', 'N', 'N', 'W', 'W'}, 0, 0, {19, 9}
		};
		struct Snake s8 = {
			{'S', 'S'}, 0, 0, {9, 25}
		};
		struct Snake s9 = {
			{'N', 'N'}, 0, 0, {11, 24}
		};
		struct Snake s10 = {
			{'S', 'S'}, 0, 0, {9, 23}
		};
		struct Fireball fb1 = {
			{10, 4}, {10, 4}, {16, 4}
		};
		struct Fireball fb2 = {
			{18, 14}, {18, 14}, {23, 14}
		};
		struct Fireball fb3 = {
			{22, 16}, {22, 16}, {17, 16}
		};
		struct Fireball fb4 = {
			{18, 18}, {18, 18}, {23, 18}
		};
		struct Fireball fb5 = {
			{8, 38}, {8, 38}, {24, 38}
		};
		struct Fireball fb6 = {
			{13, 35}, {13, 35}, {6, 35}
		};
		struct Fireball fb7 = {
			{6, 23}, {6, 23}, {6, 16}
		};
		struct Fireball fb8 = {
			{8, 19}, {8, 19}, {8, 12}
		};
		struct Warrior w1 = {
			{4, 26}, 0, 0, {1, 31, 7, 25}, {4, 26}
		};
		struct Warrior w2 = {
			{13, 18}, 0, 0, {10, 20, 16, 16}, {13, 18}
		};
		struct Spider sp1 = {
			{18, 33}, {15, 36, 21, 31}, 0
		};
		struct Spider sp2 = {
			{18, 34}, {15, 36, 21, 31}, 0
		};
		struct RacerSpider rsp1 = {
			{'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'E', 'E', 'E', 
			'E', 'E', 'E', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'W', 'W', 'S', 
			'S', 'S', 'S', 'S', 'W', 'W', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 
			'N', 'F'}, 0, {15, 22}, 0, {21, 22}, 0
		};
		struct BouncingBall bb1 = {
			0, 1, {-1}, {3, 3}
		};
		struct Worm worm1 = {
			'E',
			{
			{6, 10}
			}
		};
		//Level Variables
		int justStarted = 1;
	while ( levelGoing == 1 ) {
		tick_clock();
		playerMovement();
		//Objects
			//Doors
			keydoor( (int8_t[]){17, 1}, &kd1.keyGot, (int8_t[]){18, 2}, &kd1.doorIsOpened );
			keydoor( (int8_t[]){22,3}, &kd2.keyGot, (int8_t[]){17,7}, &kd2.doorIsOpened );
			keydoor( (int8_t[]){15,3}, &kd3.keyGot, (int8_t[]){20,12}, &kd3.doorIsOpened );
			keydoor( (int8_t[]){10,22}, &kd4.keyGot, (int8_t[]){23, 36}, &kd4.doorIsOpened );
			keydoor( (int8_t[]){11,34}, &kd5.keyGot, (int8_t[]){4,31}, &kd5.doorIsOpened );
			keydoor( (int8_t[]){16,18}, &kd6.keyGot, (int8_t[]){3,6}, &kd6.doorIsOpened );
			//Checkpoints
			checkpoints( &chk1.alreadyActivated, chk1.location, checkpoint );
			checkpoints( &chk2.alreadyActivated, chk2.location, checkpoint );
			checkpoints( &chk3.alreadyActivated, chk3.location, checkpoint );
			//Enemies
			snake( s1.movementInstructions, &s1.instruction, s1.location, 30, &s1.goingBack, 1, &playerDead );
			snake( s2.movementInstructions, &s2.instruction, s2.location, 30, &s2.goingBack, 1, &playerDead );
			snake( s3.movementInstructions, &s3.instruction, s3.location, 25, &s3.goingBack, 1, &playerDead );
			snake( s4.movementInstructions, &s4.instruction, s4.location, 25, &s4.goingBack, 1, &playerDead );
			snake( s5.movementInstructions, &s5.instruction, s5.location, 15, &s5.goingBack, 0, &playerDead );
			snake( s6.movementInstructions, &s6.instruction, s6.location, 15, &s6.goingBack, 0, &playerDead );
			snake( s7.movementInstructions, &s7.instruction, s7.location, 15, &s7.goingBack, 0, &playerDead );
			snake( s8.movementInstructions, &s8.instruction, s8.location, 25, &s8.goingBack, 1, &playerDead );
			snake( s9.movementInstructions, &s9.instruction, s9.location, 25, &s9.goingBack, 1, &playerDead );
			snake( s10.movementInstructions, &s10.instruction, s10.location, 25, &s10.goingBack, 1, &playerDead );
			spider( sp1.location, 25, &sp1.activated, sp1.roomDimensions, &playerDead );
			spider( sp2.location, 25, &sp2.activated, sp2.roomDimensions, &playerDead );
			fireball( fb1.start, 'S', fb1.end, fb1.currentLocation, 15, &playerDead );
			fireball( fb2.start, 'S', fb2.end, fb2.currentLocation, 15, &playerDead );
			fireball( fb3.start, 'N', fb3.end, fb3.currentLocation, 15, &playerDead );
			fireball( fb4.start, 'S', fb4.end, fb4.currentLocation, 15, &playerDead );
			fireball( fb5.start, 'S', fb5.end, fb5.currentLocation, 6, &playerDead );
			fireball( fb6.start, 'N', fb6.end, fb6.currentLocation, 15, &playerDead );
			fireball( fb7.start, 'W', fb7.end, fb7.currentLocation, 15, &playerDead );
			fireball( fb8.start, 'W', fb8.end, fb8.currentLocation, 15, &playerDead );
			warrior( &w1.activated, w1.currentLocation, dungeonLayout, &w1.dead, w1.room_dimensions, 25, w1.start, 1, &playerDead );
			warrior( &w2.activated, w2.currentLocation, dungeonLayout, &w2.dead, w2.room_dimensions, 30, w2.start, 1, &playerDead );
			raceSpider( rsp1.movementInstructions, &rsp1.instruction, rsp1.location, &rsp1.activated, rsp1.triggerLocation, 7, &rsp1.finished, &playerDead );
			worm( worm1.location, (int8_t[]){1, 13, 6, 7}, &worm1.direction, 18, 15, dungeonLayout, &playerDead );
			if ( rsp1.activated == 0 ) { //Hides racing spider so it's a surprise!
				mvwaddch( stdscr, 15, 22, ' ' );
			}
			bouncingBall( (int8_t[]){0, 6, 9, 1}, &bb1.horizontalDirection, &bb1.verticalDirection, 8, bb1.location, &playerDead );
		dungeonGen( dungeonLayout, &playerDead );
		#ifdef DEBUG_MODE
		if ( toggle_godmode == 1 )
			playerDead = 0;
		#endif
		if ( playerDead == 1 ) {
			wclear( stdscr );
			memcpy( new_player_cooridinates, checkpoint, sizeof(int8_t)*2 );
			memcpy( old_player_cooridinates, checkpoint, sizeof(int8_t)*2 );
			sound_player_died();
			deaths++;
			switch ( checkpoint[0] * checkpoint[1] ) {
				case 23:
					kd1.keyGot = 0;
					kd1.doorIsOpened = 0;
					kd2.doorIsOpened = 0;
					kd2.keyGot = 0;
					break;
				case 112:
					kd3.keyGot = 0;
					kd3.doorIsOpened = 0;
					rsp1.finished = 0;
					rsp1.instruction = 0;
					rsp1.activated = 0;
					memcpy( rsp1.location, (int8_t[]){15, 22}, sizeof(int8_t)*2 );
					break;
				case 364:
					kd4.keyGot = 0;
					kd4.doorIsOpened = 0;
					break;
				case 333:
					kd5.keyGot = 0;
					kd5.doorIsOpened = 0;
					kd6.keyGot = 0;
					kd6.doorIsOpened = 0;
					w1.dead = 0;
					w2.dead = 0;
					break;
				default:
					break;
			}
			playerDead = 0;
		}
		playerDraw();
		stats();
		if ( justStarted == 1 ) {
			dialogue( "Ouch. I guess I should've watched where I was stepping.\n"
					"I'm lucky I didn't die from that fall, and my sword broke too!\n"
					"I guess I better get a move on...", "Mercenary" );
			justStarted = 0;	
		}
		refresh();
	}
	return;
}
