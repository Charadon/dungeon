/*
Dungeon
Copyright (C) 2020 Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../dungeon.h"

void level3() {
	wclear( stdscr );
	int8_t playerDead = 0;
	int8_t levelGoing = 1;
	int8_t checkpoint[2] = {15, 25};
	//This variable determines the layout of the dungeon. 0 is walls, 1 is space, 2 is lava, 3 is checkpoints, 4 is clearing worms, 5 end.
	const int8_t DUNGEON_LAYOUT[] = {
/* 00 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 01 */	0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 2, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 02 */	0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
/* 03 */	0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 2, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
/* 04 */	0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0,
/* 05 */	0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
/* 06 */	0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
/* 07 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 4, 4, 4, 0, 1, 1, 1, 0, 0,
/* 08 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 2, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 4, 0, 4, 1, 1, 0, 1, 0, 0,
/* 09 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 2, 1, 0, 1, 0, 4, 4, 4, 0, 1, 1, 1, 0, 0,
/* 10 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 1, 0, 2, 2, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
/* 11 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 2, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0,
/* 12 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 1, 0, 1, 1, 1, 2, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0,
/* 13 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 2, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0,
/* 14 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0,
/* 15 */	0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
/* 16 */	0, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 17 */	0, 4, 2, 0, 0, 0, 4, 1, 4, 0, 0, 0, 2, 4, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 18 */	0, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 4, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 2, 1, 2, 0, 1, 0,
/* 19 */	0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 0,
/* 20 */	0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 21 */	0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 4, 0, 1, 0, 4, 0, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 0,
/* 22 */	0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 4, 0, 1, 0, 4, 0, 1, 1, 1, 1, 1, 2, 1, 0, 2, 1, 2, 0, 1, 0,
/* 23 */	0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 4, 4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0,
/* 24 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		/* 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 */
	level = 3;
	//Player Starting Location
	memcpy( old_player_cooridinates, (int8_t[]){15, 25}, sizeof(int8_t)*2 );
	memcpy( new_player_cooridinates, (int8_t[]){15, 25}, sizeof(int8_t)*2 );
	struct Checkpoint chk1 = {
		0, {2, 32}
	};
	struct Checkpoint chk2 = {
		0, {6, 14}
	};
	struct Checkpoint chk3 = {
		0, {9, 11}
	};
	struct Checkpoint chk4 = {
		0, {22, 18}
	};
	//Objects
	struct PushableBlock pb1 = {
		{14, 24}, {14, 24}, 0, 0, {5, 27}, {15, 28}
	};
	struct PushableBlock pb2 = {
		{21, 22}, {21, 22}, 0, 0, {17, 37}, {18, 35}
	};
	struct PushableBlock pb2_1 = {
		{21, 22}, {21, 22}, 0, 0, {17, 37}, {20, 33}
	};
	struct PushableBlock pb2_2 = {
		{21, 22}, {21, 22}, 0, 0, {17, 37}, {22, 35}
	};
	struct PushableBlock pb2_3 = {
		{21, 22}, {21, 22}, 0, 0, {17, 37}, {20, 37}
	};
	struct Keydoor key1 = {
		{1, 37}, 0, {6, 20}, 0
	};
	struct Keydoor key2 = {
		{2, 21}, 0, {6, 16}, 0
	};
	struct Keydoor key3 = {
		{18, 23}, 0, {6, 15}, 0
	};
	struct Keydoor key4 = {
		{2, 1}, 0, {7, 11}, 0
	};
	struct Keydoor key5 = {
		{3, 8}, 0, {8, 11}, 0
	};
	struct Keydoor key6 = {
		{19, 14}, 0, {19, 4}, 0
	};
	struct Keydoor key7 = {
		{20, 9}, 0, {22, 17}, 0
	};
	//Enemies
	struct Fireball fb1 = {
		{1, 17}, {1, 17}, {7, 17}
	};
	struct Fireball fb2 = {
		{5, 19}, {1, 19}, {7, 19}
	};
	struct Fireball fb3 = {
		{0, 25}, {0, 25}, {5, 25}
	};
	struct Fireball fb4 = {
		{1, 38}, {1, 38}, {1, 24}
	};
	struct Fireball fb5 = {
		{20, 1}, {20, 1}, {20, 8}
	};
	struct Fireball fb6 = {
		{21, 2}, {21, 2}, {21, 9}
	};
	struct Fireball fb7 = {
		{4, 21}, {4, 21}, {4, 26}
	};
	struct Fireball fb8 = {
		{22, 3}, {22, 3}, {22, 13}
	};
	struct Fireball fb9 = {
		{23, 4}, {23, 4}, {23, 11}
	};
	struct Fireball fb10 = {
		{4, 16}, {4, 16}, {4, 20}
	};
	struct Snake snk1 = {
		{'E','E','E','E'}, 0, 0, {8, 15}
	};
	struct Snake snk2 = {
		{'E','E','E','E'}, 0, 0, {10, 15}
	};
	struct Snake snk3 = {
		{'E','E','E','E'}, 0, 0, {12, 15}
	};
	struct Snake snk4 = {
		{'E','E','E','E'}, 0, 0, {14, 15}
	};
	struct Snake snk5 = {
		{'E','E','E','E'}, 0, 0, {16, 15}
	};
	struct Snake snk6 = {
		{'W','W','W','W'}, 0, 0, {9, 21}
	};
	struct Snake snk7 = {
		{'W','W','W','W'}, 0, 0, {11, 21}
	};
	struct Snake snk8 = {
		{'W','W','W','W'}, 0, 0, {13, 21}
	};
	struct Snake snk9 = {
		{'W','W','W','W'}, 0, 0, {15, 21}
	};
	struct Snake snk10 = {
		{'E','W','W','E'}, 0, 0, {12, 32}
	};
	struct Snake snk11 = {
		{'N','N'}, 0, 0, {23, 14}
	};
	struct Snake snk12 = {
		{'S','S'}, 0, 0, {21, 15}
	};
	struct Snake snk13 = {
		{'W','W','E','E'}, 0, 0, {9, 37}
	};
	struct Snake snk14 = {
		{'N','S','S','N'}, 0, 0, {4, 33}
	};
	struct Snake snk15 = {
		{'S','N','N','S'}, 0, 0, {4, 35}
	};
	struct Snake snk16 = {
		{'W','E','E','W'}, 0, 0, {12, 36}
	};
	struct Snake snk17 = {
		{'W','W','W','E','E','E'}, 0, 0, {5, 11}
	};
	struct Snake snk18 = {
		{'E','E','E','W','W','W'}, 0, 0, {4, 8}
	};
	struct BouncingBall bnc1 = {
		0, 1, {9, 13, 15, 0}, {10, 4}
	};
	struct BouncingBall bnc2 = {
		0, 0, {9, 13, 15, 0}, {13, 6}
	};
	struct Worm wrm1 = {
		'E',
		{
			{9, 32}
		}
	};
	struct Worm wrm2 = {
		'E',
		{
			{18, 10}
		}
	};
	struct Worm wrm3 = {
		'E',
		{
			{18, 1}
		}
	};
	struct Worm wrm4 = {
		'S',
		{
			{22, 20}
		}
	};
	struct RacerSpider rsp1 = {
		{'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'E', 'E', 'N', 'N', 'E',
		'E', 'E', 'E', 'S', 'S', 'S', 'E', 'E', 'N', 'N', 'N', 'N', 'N', 'N',
		'N', 'W', 'W', 'S', 'W', 'W', 'N', 'W', 'W', 'N', 'N', 'E', 'E', 'E',
		'E', 'E', 'E', 'N', 'N', 'W', 'W', 'W', 'W', 'W', 'F'}, 0, {4, 29}, 0,
		{15, 31}, 0
	};
	struct Warrior w1 = {
		{5, 2}, 0, 0, {0, 15, 7, 0}, {5, 2}
	};
	struct Warrior w2 = {
		{2, 2}, 0, 0, {0, 15, 7, 0}, {2, 2}
	};
	while ( levelGoing == 1 ) {
		tick_clock();
		playerMovement();
		checkpoints( &chk1.alreadyActivated, chk1.location, checkpoint );
		checkpoints( &chk2.alreadyActivated, chk2.location, checkpoint );
		checkpoints( &chk3.alreadyActivated, chk3.location, checkpoint );
		checkpoints( &chk4.alreadyActivated, chk4.location, checkpoint );
		//Objects
			pushableBlock( pb1.blockOldLoc, pb1.blockNewLoc, &pb1.doorIsOpened,
				&pb1.destroyed, DUNGEON_LAYOUT, pb1.switchLocation, pb1.doorLocation );
			//Block 2
				pushableBlock( pb2.blockOldLoc, pb2.blockNewLoc, &pb2.doorIsOpened,
					&pb2.destroyed, DUNGEON_LAYOUT, pb2.switchLocation, pb2.doorLocation );
				pushableBlock( pb2_1.blockOldLoc, pb2_1.blockNewLoc, &pb2_1.doorIsOpened,
					&pb2_1.destroyed, DUNGEON_LAYOUT, pb2_1.switchLocation, pb2_1.doorLocation );
				pushableBlock( pb2_2.blockOldLoc, pb2_2.blockNewLoc, &pb2_2.doorIsOpened,
					&pb2_2.destroyed, DUNGEON_LAYOUT, pb2_2.switchLocation, pb2_2.doorLocation );
				pushableBlock( pb2_3.blockOldLoc, pb2_3.blockNewLoc, &pb2_3.doorIsOpened,
					&pb2_3.destroyed, DUNGEON_LAYOUT, pb2_3.switchLocation, pb2_3.doorLocation );
			keydoor( key1.keyLocation, &key1.keyGot, key1.doorLocation, &key1.doorIsOpened );
			keydoor( key2.keyLocation, &key2.keyGot, key2.doorLocation, &key2.doorIsOpened );
			keydoor( key3.keyLocation, &key3.keyGot, key3.doorLocation, &key3.doorIsOpened );
			keydoor( key4.keyLocation, &key4.keyGot, key4.doorLocation, &key4.doorIsOpened );
			keydoor( key5.keyLocation, &key5.keyGot, key5.doorLocation, &key5.doorIsOpened );
			keydoor( key6.keyLocation, &key6.keyGot, key6.doorLocation, &key6.doorIsOpened );
			keydoor( key7.keyLocation, &key7.keyGot, key7.doorLocation, &key7.doorIsOpened );
		//Enemeies
			fireball( fb1.start, 'S', fb1.end, fb1.currentLocation, 7, &playerDead );
			fireball( fb2.start, 'S', fb2.end, fb2.currentLocation, 7, &playerDead );
			fireball( fb3.start, 'S', fb3.end, fb3.currentLocation, 25, &playerDead );
			fireball( fb4.start, 'W', fb4.end, fb4.currentLocation, 12, &playerDead );
			fireball( fb5.start, 'E', fb5.end, fb5.currentLocation, 15, &playerDead ); //bottom left room fireball
			fireball( fb6.start, 'E', fb6.end, fb6.currentLocation, 15, &playerDead ); //bottom left room fireball
			fireball( fb7.start, 'E', fb7.end, fb7.currentLocation, 25, &playerDead );
			fireball( fb8.start, 'E', fb8.end, fb8.currentLocation, 5, &playerDead ); //bottom left room fireball
			fireball( fb9.start, 'E', fb9.end, fb9.currentLocation, 15, &playerDead ); //bottom left room fireball
			fireball( fb10.start, 'E', fb10.end, fb10.currentLocation, 15, &playerDead );
			snake( snk1.movementInstructions, &snk1.instruction, snk1.location, 15, &snk1.goingBack, 1, &playerDead );
			snake( snk2.movementInstructions, &snk2.instruction, snk2.location, 15, &snk2.goingBack, 1, &playerDead );
			snake( snk3.movementInstructions, &snk3.instruction, snk3.location, 15, &snk3.goingBack, 1, &playerDead );
			snake( snk4.movementInstructions, &snk4.instruction, snk4.location, 15, &snk4.goingBack, 1, &playerDead );
			snake( snk5.movementInstructions, &snk5.instruction, snk5.location, 15, &snk5.goingBack, 1, &playerDead );
			snake( snk6.movementInstructions, &snk6.instruction, snk6.location, 15, &snk6.goingBack, 1, &playerDead );
			snake( snk7.movementInstructions, &snk7.instruction, snk7.location, 15, &snk7.goingBack, 1, &playerDead );
			snake( snk8.movementInstructions, &snk8.instruction, snk8.location, 15, &snk8.goingBack, 1, &playerDead );
			snake( snk9.movementInstructions, &snk9.instruction, snk9.location, 15, &snk9.goingBack, 1, &playerDead );
			snake( snk10.movementInstructions, &snk10.instruction, snk10.location, 15, &snk10.goingBack, 0, &playerDead );
			snake( snk11.movementInstructions, &snk11.instruction, snk11.location, 15, &snk11.goingBack, 1, &playerDead );
			snake( snk12.movementInstructions, &snk12.instruction, snk12.location, 15, &snk12.goingBack, 1, &playerDead );
			snake( snk13.movementInstructions, &snk13.instruction, snk13.location, 15, &snk13.goingBack, 0, &playerDead );
			snake( snk14.movementInstructions, &snk14.instruction, snk14.location, 15, &snk14.goingBack, 0, &playerDead );
			snake( snk15.movementInstructions, &snk15.instruction, snk15.location, 15, &snk15.goingBack, 0, &playerDead );
			snake( snk16.movementInstructions, &snk16.instruction, snk16.location, 15, &snk16.goingBack, 0, &playerDead );
			snake( snk17.movementInstructions, &snk17.instruction, snk17.location, 15, &snk17.goingBack, 0, &playerDead );
			snake( snk18.movementInstructions, &snk18.instruction, snk18.location, 15, &snk18.goingBack, 0, &playerDead );
			bouncingBall( bnc1.roomDimensions, &bnc1.horizontalDirection, &bnc1.verticalDirection, 6, bnc1.location, &playerDead );
			bouncingBall( bnc2.roomDimensions, &bnc2.horizontalDirection, &bnc2.verticalDirection, 6, bnc2.location, &playerDead );
			worm( wrm1.location, (int8_t[]){7,33,9,31},  &wrm1.direction, 4, 15,  DUNGEON_LAYOUT, &playerDead );
			worm( wrm2.location, (int8_t[]){16,13,18,8}, &wrm2.direction, 10, 15, DUNGEON_LAYOUT, &playerDead );
			worm( wrm3.location, (int8_t[]){16,6,18,1},  &wrm3.direction, 10, 15, DUNGEON_LAYOUT, &playerDead );
			worm( wrm4.location, (int8_t[]){20,24,23,20}, &wrm4.direction, 5, 15, DUNGEON_LAYOUT, &playerDead );
			raceSpider( rsp1.movementInstructions, &rsp1.instruction, rsp1.location, &rsp1.activated, rsp1.triggerLocation, 15, &rsp1.finished, &playerDead );
			warrior( &w1.activated, w1.currentLocation, DUNGEON_LAYOUT, &w1.dead, w1.room_dimensions, 15, w1.start, 0, &playerDead );
			warrior( &w2.activated, w2.currentLocation, DUNGEON_LAYOUT, &w2.dead, w2.room_dimensions, 15, w2.start, 0, &playerDead );
		dungeonGen( DUNGEON_LAYOUT, &playerDead);

		playerDraw();
		stats();
		wrefresh( stdscr );
		if ( playerDead == 1 ) {
			wclear( stdscr );
			switch ( checkpoint[0] * checkpoint[1] ) {
				case 375:
					memcpy( pb1.blockNewLoc, (int8_t[]){14, 24}, sizeof(int8_t)*2 );
					pb1.destroyed = 0;
					rsp1.instruction = 0;
					memcpy( rsp1.location, (int8_t[]){4, 29}, sizeof(int8_t)*2 );
					rsp1.activated = 0;
					rsp1.finished = 0;
					break;
				case 64:
					key1.doorIsOpened = 0;
					key1.keyGot = 0;
					key2.doorIsOpened = 0;
					key2.keyGot = 0;
					key3.doorIsOpened = 0;
					key3.keyGot = 0;
				case 84:
					w1.dead = 0;
					w1.activated = 0;
					memcpy( w1.currentLocation, w1.start, sizeof(int8_t)*2 );
					w2.dead = 0;
					w2.activated = 0;
					memcpy( w2.currentLocation, w2.start, sizeof(int8_t)*2 );
					key4.doorIsOpened = 0;
					key4.keyGot = 0;
					key5.doorIsOpened = 0;
					key5.keyGot = 0;
					break;
				case 99:
					key6.doorIsOpened = 0;
					key6.keyGot = 0;
					key7.doorIsOpened = 0;
					key7.keyGot = 0;
					break;
				case 396:
					memcpy( pb2.blockNewLoc, (int8_t[]){21, 22}, sizeof(int8_t)*2 );
					pb2.destroyed = 0;
					memcpy( pb2_1.blockNewLoc, (int8_t[]){21, 22}, sizeof(int8_t)*2 );
					pb2_1.destroyed = 0;
					memcpy( pb2_2.blockNewLoc, (int8_t[]){21, 22}, sizeof(int8_t)*2 );
					pb2_2.destroyed = 0;
					memcpy( pb2_3.blockNewLoc, (int8_t[]){21, 22}, sizeof(int8_t)*2 );
					pb2_3.destroyed = 0;
					break;
				default:
					fprintf( stderr, "E5: Invalid checkpoint is being used, so progress cannot be rolled back in level %d\n", level );
					break;
			}
			memcpy( new_player_cooridinates, checkpoint, sizeof(checkpoint) );
			memcpy( old_player_cooridinates, new_player_cooridinates, sizeof(new_player_cooridinates) );
			playerDead = 0;
		}
	}
}
