/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../dungeon.h"

void level2(){
	bool level2_going = TRUE;
	int8_t playerDead = 0;
	clear();
	//This variable determines the layout of the dungeon. 0 is walls, 1 is space, 2 is lava, 3 is checkpoints, 4 is clearing worms, 5 end
	const int8_t level_dungeonwalls[] = {
/* 00 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 01 */	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
/* 02 */	0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
/* 03 */	0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
/* 04 */	0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0,
/* 05 */	0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 2, 1, 1, 1, 0, 0, 2, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
/* 06 */	0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0,
/* 07 */	0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0,
/* 08 */	0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0,
/* 09 */	0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0,
/* 10 */	0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0,
/* 11 */	0, 4, 2, 2, 2, 2, 2, 4, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0,
/* 12 */	0, 4, 2, 2, 2, 2, 2, 4, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0,
/* 13 */	0, 4, 2, 2, 2, 2, 2, 4, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 2, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0,
/* 14 */	0, 4, 2, 2, 2, 2, 2, 4, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0,
/* 15 */	0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 5, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0,
/* 16 */	0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0,
/* 17 */	0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 0, 2, 1, 2, 0, 1, 0, 0, 0, 1, 1, 2, 0, 1, 0, 0,
/* 18 */	0, 2, 2, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0,
/* 19 */	0, 1, 1, 1, 2, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 1, 2, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0,
/* 20 */	0, 1, 1, 1, 2, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0,
/* 21 */	0, 2, 1, 2, 2, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 22 */	0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 23 */	0, 2, 1, 1, 2, 0, 1, 0, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
/* 24 */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		/* 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 */

	struct Keydoor k1 = {
		{7, 8}, 0, {16, 6}, 0
	};
	struct Keydoor k2 = {
		{18, 26}, 0, {12, 24}, 0
	};
	struct Keydoor k3 = {
		{8, 16}, 0, {11, 33}, 0
	};
	struct Fireball fb1 = {
		{1, 33}, {1, 33}, {9, 33}
	};
	struct Fireball fb2 = {
		{12, 36}, {12, 36}, {1, 36} 
	};
	struct Snake s1 = {
		{'W'}, 0, 0, {7, 2}
	};
	struct Snake s2 = {
		{'W'}, 0, 0, {5, 2}
	};
	struct Snake s3 = {
		{'W'}, 0, 0, {3, 2}
	};
	struct Snake s4 = {
		{'W'}, 0, 0, {7, 5}
	};
	struct Snake s5 = {
		{'W'}, 0, 0, {5, 5}
	};
	struct Snake s6 = {
		{'W'}, 0, 0, {3, 5}
	};
	struct Snake snake9 = {
		{'W', 'W', 'S', 'S', 'E', 'E', 'N', 'N'}, 0, 0, {10, 28}
	};
	struct Snake snake10 = {
		{'N', 'N', 'E', 'E', 'S', 'S', 'W', 'W'}, 0, 0, {9, 28}
	};
	struct Snake _11 = {
		{'E', 'E', 'S', 'S', 'W', 'W', 'N', 'N'}, 0, 0, {10, 30}
	};
	struct Spider sp1 = {
		{5, 8}, {2,10,8,6}, 0
	};
	struct Fireball_withPattern fp1 = {
		{15, 10}, {15, 10}, {23, 10}, -1
	};
	struct Fireball_withPattern fp2 = {
		{22, 28}, {22, 28}, {22, 12}, -1
	};
	struct Fireball_withPattern fp3 = {
		{22, 28}, {22, 28}, {22, 39}, -1
	};
	struct Warrior w1 = {
		{15, 21}, 0, 0, {11, 23, 19, 15}, {15, 21}
	};
	struct Warrior w2 = {
		{13, 17}, 0, 0, {11, 23, 19, 15}, {13, 17}
	};
	struct Warrior w3 = {
		{18, 19}, 0, 0, {11, 23, 19, 15}, {18, 19}
	};
	struct RacerSpider rs1 = {
		{'W', 'W', 'W', 'W', 'W', 'S', 'S', 'S', 'W', 'W', 'N', 'N', 'N', 'N', 
		'W', 'W', 'W', 'W', 'W', 'W', 'W', 'S', 'S', 'F'}, 0, {3, 29}, 0, 
		{5, 28}, 0
	};
	struct BouncingBall bb1 = {
		0, 0, {0, 14, 9, 10}, {5, 12} 
	};
	struct PushableBlock pb1 = {
		{22, 3}, {22, 3}, 0, 0, {17, 1}, {22, 7}
	};
	struct PushableBlock pb2 = {
		{8, 18}, {8, 18}, 0, 0, {-1}, {-1}
	};
	struct PushableBlock pb3 = {
		{18, 34}, {18, 34}, 0, 0, {14, 28}, {15, 27}
	};
	struct Checkpoint chk1 = {
		0, {22, 8}	
	};
	struct Checkpoint chk2 = {
		0, {12, 25}	
	};
	struct Checkpoint chk3 = {
		0, {13, 38}
	};
	struct Worm worm1 = {
		'W', 
		{
		{10, 3}
		}
	};
	// Sets player's starting location
	memcpy( old_player_cooridinates, (int8_t[]){8, 1}, sizeof(int8_t)*2 );
	memcpy( new_player_cooridinates, (int8_t[]){8, 1}, sizeof(int8_t)*2 );
	//Debug
	//memcpy( old_player_cooridinates, (int8_t[]){17, 13}, sizeof(int8_t)*2 );
	//memcpy( new_player_cooridinates, (int8_t[]){17, 13}, sizeof(int8_t)*2 );
	//Sets initial Checkpoint
	memcpy( checkpoint, (int8_t[]){8, 1}, sizeof(int8_t)*2 );
	level = 2;
	while (level2_going == TRUE){
		tick_clock();
		term_nagScreen();
		playerMovement();
		//Objects
			keydoor( k1.keyLocation, &k1.keyGot, k1.doorLocation, &k1.doorIsOpened );
			keydoor( k2.keyLocation, &k2.keyGot, k2.doorLocation, &k2.doorIsOpened );
			keydoor( k3.keyLocation, &k3.keyGot, k3.doorLocation, &k3.doorIsOpened );
			pushableBlock( pb1.blockOldLoc, pb1.blockNewLoc, &pb1.doorIsOpened, &pb1.destroyed, level_dungeonwalls, pb1.switchLocation, pb1.doorLocation );
			pushableBlock( pb2.blockOldLoc, pb2.blockNewLoc, &pb2.doorIsOpened, &pb2.destroyed, level_dungeonwalls, pb2.switchLocation, pb2.doorLocation );
			pushableBlock( pb3.blockOldLoc, pb3.blockNewLoc, &pb3.doorIsOpened, &pb3.destroyed, level_dungeonwalls, pb3.switchLocation, pb3.doorLocation );
			checkpoints( &chk1.alreadyActivated, chk1.location, checkpoint );
			checkpoints( &chk2.alreadyActivated, chk2.location, checkpoint );
			checkpoints( &chk3.alreadyActivated, chk3.location, checkpoint );
		//Enemies
			worm( worm1.location, (int8_t[]){10, 7, 15, 1}, &worm1.direction, 19, 15, level_dungeonwalls, &playerDead );
			spider( sp1.location, 25, &sp1.activated, sp1.roomDimensions, &playerDead );
			fireball( fb1.start, 'S', fb1.end, fb1.currentLocation, 10, &playerDead );
			fireball( fb2.start, 'N', fb2.end, fb2.currentLocation, 5, &playerDead );
			snake( s1.movementInstructions, &s1.instruction, s1.location, 30, &s1.goingBack, 1, &playerDead );
			snake( s2.movementInstructions, &s2.instruction, s2.location, 30, &s2.goingBack, 1, &playerDead );
			snake( s3.movementInstructions, &s3.instruction, s3.location, 30, &s3.goingBack, 1, &playerDead );
			snake( s4.movementInstructions, &s4.instruction, s4.location, 30, &s4.goingBack, 1, &playerDead );
			snake( s5.movementInstructions, &s5.instruction, s5.location, 30, &s5.goingBack, 1, &playerDead );
			snake( s6.movementInstructions, &s6.instruction, s6.location, 30, &s6.goingBack, 1, &playerDead );
			snake( snake9.movementInstructions, &snake9.instruction, snake9.location, 15, &snake9.goingBack, 0, &playerDead );
			snake( snake10.movementInstructions, &snake10.instruction, snake10.location, 15, &snake10.goingBack, 0, &playerDead );
			snake( _11.movementInstructions, &_11.instruction, _11.location, 15, &_11.goingBack, 0, &playerDead );
			fireball_withPattern( 'S', fp1.location, fp1.start, fp1.end, 8, &fp1.pattern, &playerDead );
			fireball_withPattern( 'W', fp2.location, fp2.start, fp2.end, 6, &fp2.pattern, &playerDead );
			fireball_withPattern( 'E', fp3.location, fp3.start, fp3.end, 8, &fp3.pattern, &playerDead );
			warrior( &w1.activated, w1.currentLocation, level_dungeonwalls, &w1.dead, w1.room_dimensions, 50, w1.start, 1, &playerDead );
			warrior( &w2.activated, w2.currentLocation, level_dungeonwalls, &w2.dead, w2.room_dimensions, 50, w2.start, 1, &playerDead );
			warrior( &w3.activated, w3.currentLocation, level_dungeonwalls, &w3.dead, w3.room_dimensions, 50, w3.start, 1, &playerDead );
			raceSpider( rs1.movementInstructions, &rs1.instruction, rs1.location, &rs1.activated, rs1.triggerLocation, 8, &rs1.finished, &playerDead );
			bouncingBall( bb1.roomDimensions, &bb1.horizontalDirection, &bb1.verticalDirection, 6, bb1.location, &playerDead );
		dungeonGen( level_dungeonwalls, &playerDead );
		#ifdef DEBUG_MODE
		if ( toggle_godmode == 1 )
			playerDead = 0;
		#endif
		if ( playerDead == 1 ) {
			memcpy( new_player_cooridinates, checkpoint, sizeof(int8_t)*2 );
			deaths++;
			sound_player_died();
			switch ( checkpoint[0] * checkpoint[1] ) {
				case 8:
					memcpy( sp1.location, (int8_t[]){5, 8}, sizeof(int8_t)*2 );
					k1.keyGot = 0;
					k1.doorIsOpened = 0;
					pb1.destroyed = 0;
					pb1.doorIsOpened = 0;
					memcpy( pb1.blockNewLoc, (int8_t[]){22, 3}, sizeof(int8_t)*2 );
					memcpy( pb1.blockOldLoc, (int8_t[]){22, 3}, sizeof(int8_t)*2 );
					break;
				case 176:
					k2.keyGot = 0;
					k2.doorIsOpened = 0;
					memcpy( w1.currentLocation, (int8_t[]){15, 21}, sizeof(int8_t)*2 );
					memcpy( w2.currentLocation, (int8_t[]){13, 17}, sizeof(int8_t)*2 );
					memcpy( w3.currentLocation, (int8_t[]){18, 18}, sizeof(int8_t)*2 );
					w1.dead = 0;
					w2.dead = 0;
					w3.dead = 0;
					break;
				case 300:
					k3.doorIsOpened = 0;
					k3.keyGot = 0;
					memcpy( rs1.location, (int8_t[]){3, 29}, sizeof(int8_t)*2 );
					rs1.activated = 0;
					rs1.finished = 0;
					rs1.instruction = 0;
					memcpy( pb2.blockNewLoc, (int8_t[]){8, 18}, sizeof(int8_t)*2 );
					memcpy( pb2.blockOldLoc, (int8_t[]){8, 18}, sizeof(int8_t)*2 );
					break;
				case 494:
					pb3.doorIsOpened = 0;
					pb3.destroyed = 0;
					memcpy( pb3.blockNewLoc, (int8_t[]){18, 34}, sizeof(int8_t)*2 );
					memcpy( pb3.blockOldLoc, (int8_t[]){18, 34}, sizeof(int8_t)*2 );
					break;
				default:
					break;
			}
			clear();
			playerDead = 0;
		}
		//Draw Objects
		playerDraw();
		stats();
		refresh();
	}
	return;
}
