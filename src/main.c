/*
Dungeon
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "dungeon.h"

//Initialize Variables
	int8_t old_player_cooridinates[2] = {0, 0};
	int8_t new_player_cooridinates[2] = {0, 0};
	int8_t checkpoint[] = {0, 0};
	uint16_t deaths = 0;
	int64_t tick = 0;
	int8_t level = 1;
	#ifdef DEBUG_MODE
		int toggle_dungeon_walls = 0;
		int toggle_time = 0;
		int toggle_godmode = 0;
	#endif

void initialize_terminal(){
	curs_set(0); //Makes terminal cursor invisible.
	return;
}

void initialize_audio(){
	InitAudioDevice();
	doorlocked_sound = LoadSound("sounds/door_locked.ogg");
	dooropening_sound = LoadSound("sounds/door_opening.ogg");
	checkpoint_sound = LoadSound("sounds/checkpoint.ogg");
	stairs_sound = LoadSound("sounds/going_up_stairs.ogg");
	playerdead_sound = LoadSound("sounds/dead.ogg");
	return;
}

void initialize_color(){
	start_color();
	/*           Foreground   Background */
	init_pair(1, COLOR_WHITE, COLOR_BLACK); //Player's Color
	init_pair(2, COLOR_WHITE, COLOR_WHITE); //Wall's Color
	init_pair(3, COLOR_YELLOW, COLOR_BLACK); //Key Color
	init_pair(4, COLOR_CYAN, COLOR_CYAN); //Door Color
	init_pair(5, COLOR_YELLOW, COLOR_RED); //Lava Color
	init_pair(6, COLOR_RED, COLOR_BLACK); //Enemy Color
	init_pair(7, COLOR_GREEN, COLOR_GREEN); //Checkpoint Color
	init_pair(8, COLOR_GREEN, COLOR_BLACK); //Stairs Color
	init_pair(9, COLOR_BLACK, COLOR_BLACK); //Space Color
	init_pair(10, COLOR_CYAN, COLOR_BLUE); //Water Color; Will probably remain unused.
	init_pair(11, COLOR_MAGENTA, COLOR_MAGENTA); //Pushable Block
	init_pair(12, COLOR_BLUE, COLOR_BLUE); //Switch
	init_pair( 13, COLOR_WHITE, COLOR_GREEN ); //Checkpoint with player inside.
	init_pair( 14, COLOR_CYAN, COLOR_BLACK );
	return;
}

void end_game(){
	endwin();
	CloseAudioDevice();
	printf("Thank you for playing my game. Feel free to submit suggestions, bug reports, or give tips on how to make my code better on the gitlab page. =)");
	exit(0);
}

void begin_user_input(){
	echo(); //Turns on echoing
	curs_set(1); //Sets cursor to visible.
	nodelay(stdscr, FALSE); //Turns off nodelay for user input.
	return;
}

void end_user_input(){
	noecho(); //Turns off echoing
	curs_set(0); //Sets cursor to invisible.
	nodelay(stdscr, TRUE); //Turns on nodelay for user input.
	return;
}

void tick_clock(){ //50 ticks is one second
	usleep(20000);
	tick++;
	if (tick == 9223372036854775807){ //Buffer overflow protection
		tick = 1;
	}
	if (tick <= 0){
		tick = 1;
	}
	return;
}

void stats(){
	mvprintw(0, 41, "Deaths: %i", deaths);
	mvprintw(1, 41, "Floor: %i", level);
	mvprintw(2, 41, "                     ");
	mvprintw(2, 41, "Ticks: %li", tick);
	return;
}

int main(){
	initscr(); //Initializes Screen
	if ( has_colors() == FALSE ) {
		printw("Your terminal does not support color. Closing game.");
		getch();
		endwin();
		exit(0);
	}
	int y, x;
	getmaxyx( stdscr, y, x );
	while ( y < 35 || x < 65 )
		mvprintw( 0, 0, "Terminal Screen is not big enough. Resize Terminal." );
	wclear( stdscr );
	wresize( stdscr, 35, 65);
	srand( time(NULL) ); //Generates seed for rand
	keypad(stdscr, TRUE); //Allow input from special keys such as arrow keys
	raw();
	noecho(); //Prevents key presses from showing on screen.
	nodelay(stdscr, TRUE); //Prevents getch from holding up the game
	initialize_terminal();
	initialize_audio();
	initialize_color();
	title_screen();
	return 0;
}
